from django import forms
from mm.models import Season, League

class LeagueFilterForm(forms.Form):
    name = forms.CharField(label='Player', required=False)
    season = forms.ModelChoiceField(queryset=Season.objects.all(), required=False)
    league = forms.ModelChoiceField(queryset=League.objects.all(), required=False, initial=1)