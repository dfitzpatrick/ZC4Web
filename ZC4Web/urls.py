"""ZC4Web URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url, include
from django.conf import settings
from django.conf.urls.static import static
from core import views as core_views
from match import views as match_views
from leaderboard import views as leaderboard_views

urlpatterns = [

    url('admin/', admin.site.urls),
    url('api/', include('api.urls')),
    url('mm/', include('mm.urls')),
    url(r'ws/', include('zclsockets.urls')),
    url(r'^$', core_views.index, name='index'),
    url(r'^stats/$', core_views.stats_list, name='old-stats'),
    url(r'^matches/(?P<pk>\d+)/$', match_views.MatchDetailView.as_view(), name='match-detail'),
    url(r'^stats/(?P<pk>\d+)/$', core_views.stats_detail, name='stats-detail'),
    url(r'^leaderboard/$', leaderboard_views.leaderboard, name='leaderboard'),
    url(r'^streaming/$', core_views.streaming_setup, name='streaming'),
    url(r'^callouts/', include('zcallouts.urls', namespace='zcallouts')),
    url(r'^join/$', core_views.join, name='discord-join')

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]
