from decimal import Decimal
from django.shortcuts import render
from django.db.models import F, DecimalField, When, Case, IntegerField

from .models import Leaderboard
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models.functions import Rank
from django.db.models.expressions import Window
import re
from datetime import timedelta, datetime
import pytz


def make_timedelta(last_active: str) -> timedelta:
    """
    Format is
    1w2d1h18m2s

    :param last_active:
    :return:
    """
    pattern = '^(?:(?P<weeks>\d+)[w.])?(?:(?P<days>\d+)[d.])?(?:(?P<hours>\d+)[h.])?(?:(?P<minutes>\d+)[m.])?(?:(?P<seconds>\d+)[s.])?$'
    pattern = re.compile(pattern)
    matches = re.search(pattern, last_active)
    if matches is None:
        return timedelta(weeks=0, days=0, hours=0, minutes=0, seconds=0)

    args = {k: int(v) for k, v in matches.groupdict().items() if v and v.isdigit()}
    return timedelta(**args)

def leaderboard(request):

    rank_window = Window(expression=Rank(),
                         order_by=F('elo').desc())
    leaderboard = (
            Leaderboard
                .objects

                .order_by('-elo')
                .annotate(losses=F("games") - F("wins"))
                .annotate(rank=rank_window)
                .annotate(win_rate=Case(
                    When(games=0, then=0),
                    default=(Decimal('1.0') * F("wins") / F("games")) * 100,
                    output_field=DecimalField(),
                    )
                )
            )
    page = -1

    try:
        last_active = request.GET.get('after')

        if last_active:
            values = leaderboard.all().values('pk', 'rank')
            mappings = dict((p['pk'], p['rank']) for p in values)
            whens = [When(pk=pk, then=rank) for pk, rank in mappings.items()]

            delta = datetime.now(pytz.timezone('US/Central')) - make_timedelta(last_active)
            leaderboard = (
                leaderboard
                .annotate(rank=Case(
                    *whens, default=0, output_field=IntegerField())
                ).filter(updated__gte=delta)
            )



        search_player = request.GET.get('search')
        if search_player:
            values = leaderboard.all().values('pk', 'rank')
            mappings = dict((p['pk'], p['rank']) for p in values)
            whens = [When(pk=pk, then=rank) for pk, rank in mappings.items()]
            leaderboard = (
                leaderboard
                .annotate(rank=Case(
                    *whens, default=0, output_field=IntegerField())
                )
                .filter(player_name__icontains=search_player)
            )

        paginator = Paginator(leaderboard, 50)
        page = request.GET.get('page')
        players = paginator.page(page)

    except PageNotAnInteger:
        players = paginator.page(1)
    except EmptyPage:
        players = paginator.page(paginator.num_pages)
    if hasattr(page, 'isdigit') and page.isdigit():
        page = int(page)
        from_last = paginator.num_pages - page
        from_first = 0 if page - 1 < 0 else page - 1
        if from_last < 5:
            page_range = range(max(1, page-(10-from_last-1)), page+from_last+1)
        else:
            if from_first <= 4:
                page_range = range(1, min(11, paginator.num_pages+1))
            else:
                page_range = range(max(1, page - 4), min(paginator.num_pages+1, (page+5)))
    else:
        page_range = range(1, min(paginator.num_pages+1, 11))

    return render(request, 'leaderboard/leaderboard.html', {
        'leaderboard': players,
        'current_page': page if page else 1,
        'total_pages': paginator.num_pages,
        'page_range': page_range
    })