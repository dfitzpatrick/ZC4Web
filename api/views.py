from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK
from match import models as match_models
from moderation import models as mod_models
from players import models as player_models
from . import serializers
from .serializers import MatchSerializer
from collections import defaultdict
from .permissions import IsStaffOrReadOnly
from rest_framework import permissions
from django.db.models import Count
from mm import models as mm_models


class TournamentView(viewsets.ModelViewSet):
    serializer_class = serializers.TournamentSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsStaffOrReadOnly)
    queryset = match_models.Tournament.objects.all()


class PlayerView(viewsets.ModelViewSet):
    serializer_class = serializers.PlayersSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsStaffOrReadOnly)

    def get_queryset(self):
        queryset = match_models.Player.objects.all()
        return queryset


class DiscordPlayerView(viewsets.ModelViewSet):
    lookup_field = 'discord_id'
    serializer_class = serializers.PlayersSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsStaffOrReadOnly)

    def get_queryset(self):
        queryset = match_models.Player.objects.all()
        return queryset


class TeamView(viewsets.ModelViewSet):
    serializer_class = serializers.TeamSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsStaffOrReadOnly)

    def get_queryset(self):
        queryset = match_models.Team.objects.all()
        queryset = queryset.prefetch_related('players')
        return queryset


class LeagueView(viewsets.ModelViewSet):
    serializer_class = serializers.LeagueSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsStaffOrReadOnly)

    def get_queryset(self):
        queryset = match_models.League.objects.all()
        queryset = queryset.select_related('owner')
        queryset = queryset.prefetch_related('hosts', 'membership')
        return queryset


class MatchView(viewsets.ModelViewSet):
    serializer_class = MatchSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsStaffOrReadOnly)

    def get_queryset(self):
        queryset = match_models.Match.objects.all()
        queryset = queryset.select_related('tournament', 'league', 'winning_team')
        queryset = queryset.prefetch_related('teams', 'teams__players', 'winning_team__players')
        queryset = queryset.prefetch_related('league__hosts', 'league__membership', 'league__owner')

        d_id = self.request.query_params.get('discordid', None)
        if d_id is not None:
            queryset = queryset.filter(teams__players__discord_id=d_id)

        current_match = self.request.query_params.get('current', None)
        if current_match is not None:
            queryset = queryset.filter(status='in progress')
        return queryset

class EventView(viewsets.ModelViewSet):
    serializer_class = serializers.EventSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsStaffOrReadOnly)

    def get_queryset(self):
        queryset = match_models.Event.objects.all()
        queryset = queryset.select_related('host', 'league')
        queryset = queryset.select_related('league__owner')
        queryset = queryset.prefetch_related('players_interested', 'players_checked_in')
        return queryset

class InfractionView(viewsets.ModelViewSet):
    serializer_class = serializers.InfractionSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsStaffOrReadOnly)

    def get_queryset(self):
        queryset = mod_models.Infraction.objects.all()
        queryset = queryset.select_related('player', 'author')
        d_id = self.request.query_params.get('discordid', None)
        if d_id is not None:
            queryset = queryset.filter(player__discord_id=d_id)
        return queryset



class HandleView(viewsets.ModelViewSet):
    lookup_field = 'handle'
    serializer_class = serializers.HandleSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsStaffOrReadOnly)

    def get_queryset(self):
        queryset = mm_models.Handle.objects.select_related('player').all()
        return queryset

class CurrentMatchSummaryView(viewsets.ViewSet):
    #TODO: Need to optimize queries for CurrentMatchSummaryView API.

    def list(self, request, *args, **kwargs):
        def get_stats(t: match_models.Team) -> dict:
            seq = t.team_matches.filter(ranked=True)
            return {
                'wins': len([m for m in seq if m.winning_team == t]),
                'losses': len([m for m in seq if m.winning_team != t and m.winning_team is not None]),
                'draws': len([m for m in seq if not m.winning_team])
            }
        def stats_as_string(d: dict) -> str:
            return f"{d['wins']} Wins / {d['losses']} Losses / {d['draws']} Draws"

        queryset = match_models.Match.objects.all().select_related('tournament', 'league', 'event', 'winning_team')
        match = queryset.filter(status='in progress').order_by('-created').first()
        if not match:
            return Response({"error": "No Match"}, HTTP_200_OK)

        koth_winner = 'No Winner'
        two_teams = match_models.Match.objects.annotate(num_teams=Count('teams')).filter(
            status='completed', num_teams=2).order_by('-created')
        for k in two_teams:
            t_winner = k.winning_team
            if t_winner:
                if len(t_winner.players.all()) == 1:
                    koth_winner = t_winner.players.all()[0].display_name



        winning_players = None
        last_winning_team = None
        event = match.event
        num_matches = 'ad hoc Game'
        event_id = 'ad hoc Game'
        tournament_name = match.tournament.name if match.tournament else "No Season/Bracket"
        league_name = match.league.name if match.league else "All Leagues"
        if event:
            event_id = event.id
            num_matches = len(event.event_matches.all())
            completed_event_matches = event.event_matches.filter(status='completed').order_by('-created').first()
            if completed_event_matches:
                last_winning_team = completed_event_matches.winning_team
            if last_winning_team:
                winning_players = " & ".join(p.display_name for p in last_winning_team.players.all())
        else:

            completed_matches = match_models.Match.objects.filter(status='completed').order_by('-created').first()
            if completed_matches:
                last_winning_team = completed_matches.winning_team
            if last_winning_team:
                winning_players = " & ".join(p.display_name for p in last_winning_team.players.all())

        context = {
            'event_id': event_id,
            'num_matches': num_matches,
            'tournament_name': tournament_name,
            'league_name': league_name,
            'previous_winner': winning_players or "No Previous Winners",
            'koth_winner': koth_winner,

        }
        stats = defaultdict(dict)
        game_type = []
        for t_num, t in enumerate(match.teams.all()):
            player_stats = {'wins': 0, 'losses': 0, 'draws': 0}
            team_key = f"Team {t_num+1}"
            stats[team_key] = get_stats(t)
            stats[team_key]['players'] = {}
            context[f"team{t_num+1}_stats"] = stats_as_string(get_stats(t))
            game_type.append(len(t.players.all()))
            for p_num, p in enumerate(t.players.all()):
                for player_team in p.team_set.all():
                    s = get_stats(player_team)
                    player_stats['wins'] += s['wins']
                    player_stats['losses'] += s['losses']
                    player_stats['draws'] += s['draws']
                stats[team_key]['players'][p.display_name] = player_stats
                key = f"team{t_num+1}_player{p_num+1}"
                context[key] = p.display_name
                context[f"{key}_stats"] = stats_as_string(player_stats)
        context['game_type'] = 'v'.join(map(str, game_type))

        ticker = ""
        ticker_players = ""
        spacer = "      "
        for team_name in stats:
            ticker += f"{team_name}: {stats_as_string(stats[team_name])}"
            ticker += spacer
            for player_name in stats[team_name]['players']:
                ticker_players += f"{player_name}: {stats_as_string(stats[team_name]['players'][player_name])}"
                ticker_players += spacer
        ticker += ticker_players
        context['ticker'] = ticker
        context['stats'] = stats

        return Response(context, HTTP_200_OK)





