from django.db import models
from django.db.models.signals import m2m_changed
from django.dispatch import receiver
from django.db.utils import IntegrityError
from django.contrib.auth import get_user_model
from players.models import Player


import logging
from collections import Sequence

log = logging.getLogger(__name__)


# Create your models here.

STATUS_CHOICES = (
    ('in progress', 'IN PROGRESS'),
    ('completed', 'COMPLETED'),
    ('cancelled', 'CANCELLED'),
)
def get_super_user():
    o = get_user_model().objects.get(is_superuser=True)
    if isinstance(o, Sequence):
        return o[0]
    return o


class League(models.Model):
    created = models.DateTimeField(auto_created=True, auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    name = models.CharField(max_length=300)
    description = models.TextField(blank=True, null=True)
    status = models.TextField(max_length=300, choices=STATUS_CHOICES,
                              default='in progress')
    owner = models.ForeignKey(Player, related_name='owners',
                              on_delete=models.SET(get_super_user))
    hosts = models.ManyToManyField(Player, related_name='hosts', blank=True)
    membership = models.ManyToManyField(Player, related_name='league_membership', blank=True)

    def __str__(self):
        return self.name


class Event(models.Model):
    created = models.DateTimeField(auto_created=True, auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    host = models.ForeignKey(Player, on_delete=models.CASCADE)
    event_schedule = models.DateTimeField()
    status = models.CharField(max_length=30, choices=STATUS_CHOICES)
    league = models.ForeignKey(League, null=True, blank=True, default=None,
                               on_delete=models.SET_NULL)
    players_interested = models.ManyToManyField(Player,
                                                related_name='interested',
                                                blank=True)
    title = models.CharField(max_length=300, default='New Event')
    description = models.TextField(null=True, blank=True)
    players_checked_in = models.ManyToManyField(Player,
                                                related_name='checked_in',
                                                blank=True)

    def __str__(self):
        return f"{self.title} - {self.created}"

    class Meta:
        ordering = ['event_schedule']


class Tournament(models.Model):
    created = models.DateTimeField(auto_created=True, auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    name = models.CharField(max_length=300)
    status = models.TextField(max_length=300, choices=STATUS_CHOICES,
                              default='in progress')

    def __str__(self):
        return self.name


class Team(models.Model):
    created = models.DateTimeField(auto_created=True, auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    players = models.ManyToManyField(Player)

    def __str__(self):

        if self.id:
            s = '/'.join(p.display_name for p in self.players.all())
            return s
        else:
            return "No Team"

    def is_duplicate(self, players=None):
        log.info('In Method is Duplicate')
        new_team = players if players else players.objects.all()
        new_team = [p.username for p in new_team]
        other_teams = Team.objects.all()

        for ot in other_teams:

            ot = [p.username for p in ot.players.all()]
            log.debug(f'Checking {new_team} with {ot}')
            if set(new_team) == set(ot):
                return True
        return False


class Match(models.Model):
    created = models.DateTimeField(auto_created=True, auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    tournament = models.ForeignKey(Tournament, null=True, blank=True,
                                   on_delete=models.SET_NULL)
    league = models.ForeignKey(League, null=True, blank=True, default=None,
                               on_delete=models.SET_NULL,
                               related_name='league_matches')
    event = models.ForeignKey(Event, null=True, blank=True, default=None,
                               on_delete=models.SET_NULL,
                               related_name='event_matches')

    status = models.TextField(max_length=300, choices=STATUS_CHOICES,
                                   default='in progress')
    teams = models.ManyToManyField(Team, related_name='team_matches')
    stream_url = models.URLField(null=True, blank=True)
    winning_team = models.ForeignKey(Team, null=True, blank=True,
                                     related_name='winning_team_matches',
                                     on_delete=models.CASCADE)
    ranked = models.BooleanField(default=True)

    def __str__(self):
        return f"{' VS. '.join(t.__str__() for t in self.teams.all())}"


    class Meta:
        ordering = ['created']
