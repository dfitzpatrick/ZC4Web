from mm.models import Match, MatchEvent
from django.contrib.postgres.aggregates import StringAgg
from django.shortcuts import render
from django.db.models import Q, OuterRef, Subquery, CharField, TextField, When, Case
"""
Match views dedicated to the /match/ endpoint
"""

def list_view(request):

    winners_query = (
        MatchEvent
            .objects
            .filter(
            match=OuterRef('id'),
            key='WIN',
        )
    ).values('handle__name')

    matches = (
        Match
        .objects
        .select_related('league', 'season')
        .prefetch_related('rosters')
        .filter(
            ranked=True
        )
        .annotate(
            match_players=StringAgg(
                'rosters__handle__name',
                delimiter=', '
            ),

        )
        .order_by('-created')
    )

    context = {
        'matches': matches
    }
    return render(request, 'mm/match/match_view.html', context)

