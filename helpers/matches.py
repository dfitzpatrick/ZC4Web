from mm.models import Match
def set_game_id_to_null():
    """
    Looks for empty string and sets them to null as part of migrations
    :return:
    """
    matches = Match.objects.filter(game_id='')
    for m in matches:
        m.game_id = None
        m.save()
