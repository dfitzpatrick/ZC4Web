from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings


from zcallouts import views

app_name = 'zcallouts'
urlpatterns = [

    url(r'^$', views.circles, name='circles'),
    url(r'currentmatch', views.current_match, name='current_match'),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)