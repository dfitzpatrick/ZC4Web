from django import forms
from match.models import Tournament


class TournamentSelectForm(forms.Form):
    tournament = forms.ModelChoiceField(widget=forms.Select(attrs={'class': 'form-control'}),
                                        queryset=Tournament.objects.filter(status='in progress').order_by('created'),
                                        required=False)

class TournamentSelectFor2(forms.Form):
    tournament = forms.ChoiceField(widget=forms.Select(attrs={'class': 'form-control'}),
                                   label="Choose Tournament",
                                   choices=[(t.id, t.name)
                                            for t in Tournament.objects.filter(status='in progress').order_by('created')
                                            ] + [("", "Unclassified Matches")],
                                   initial=0)

    def __init__(self, *args, **kwargs):
        super(TournamentSelectForm, self).__init__(*args, **kwargs)
        self.fields['tournament'].initial = Tournament.objects.filter(status='in progress').order_by('created').first()
