from django.conf.urls import url, include
from rest_framework import routers

from . import views

router = routers.DefaultRouter()
router.register('infraction', views.InfractionView, base_name='infraction')
router.register('match', views.MatchView, base_name='match')
router.register('tournament', views.TournamentView, base_name='tournament')
router.register('player', views.PlayerView, base_name='player')
router.register('team', views.TeamView, base_name='team')
router.register('league', views.LeagueView, base_name='league')
router.register('discordplayer', views.DiscordPlayerView, base_name='discordplayer')
router.register('event', views.EventView, base_name='event')
router.register('currentmatchsummary', views.CurrentMatchSummaryView, base_name='currentmatchsummary')
router.register('handle', views.HandleView, base_name='handle')
urlpatterns = [
    url('', include(router.urls)),
]
