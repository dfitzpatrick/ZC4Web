from django.db import models
from django.contrib.postgres.aggregates import StringAgg
from players.models import Player
# Create your models here.

STATUS = (
    ('in progress', 'In Progress'),
    ('completed', 'Completed'),
)
POSITIONS = (
    (0, 'Top Left - Bottom'),
    (1, 'Top Left - Top'),
    (2, 'Top Right - Top'),
    (3, 'Top Right - Bottom'),
    (4, 'Bottom Right - Top'),
    (5, 'Bottom Right - Bottom'),
    (6, 'Bottom Left - Bottom'),
    (7, 'Bottom Left - Top'),
)
EVENT_KEYS = (
    ('player_leave', "Player Leaving"),
    ('bunker_killed', "Bunker Destroyed"),
    ('bunker started', "Bunker Building"),
    ('match_end', "Match Ending"),
    ('bunker_cancelled', "Bunker Cancelled"),
    ('match_start', "Match Starting"),
    ('WIN', 'Player Wins'),
    ('LOSS', 'Player Loses'),
    ('DRAW', 'Player Draws'),
    ('ADJ', 'Player Point Adjustment'),
)
TEAMS = (
    (0, 'Team One'),
    (1, 'Team Two'),
    (2, 'Team Three'),
    (3, 'Team Four'),
)
LANES = {
    0: 7,
    1: 2,
    2: 1,
    3: 4,
    4: 3,
    5: 6,
    6: 5,
    7: 0
}

class Handle(models.Model):
    created = models.DateTimeField(auto_created=True, auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    player = models.ForeignKey(Player, blank=True, null=True, on_delete=models.SET_NULL, related_name='handles')
    handle = models.CharField(max_length=100, unique=True)
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Season(models.Model):
    created = models.DateTimeField(auto_created=True, auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=200)
    description = models.TextField(blank=True)

    def  __str__(self):
        return self.name

class League(models.Model):
    created = models.DateTimeField(auto_created=True, auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=200)
    description = models.TextField(blank=True)

    def  __str__(self):
        return self.name


class Match(models.Model):
    created = models.DateTimeField(auto_created=True, auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    season = models.ForeignKey('Season', blank=True, null=True, on_delete=models.SET_NULL, related_name='matches')
    league = models.ForeignKey('League', blank=True, null=True, on_delete=models.SET_NULL, related_name='matches')
    game_id = models.CharField(max_length=100, blank=True, null=True, unique=True)
    streamer_id = models.CharField(max_length=100, blank=True)
    stream_url = models.URLField(null=True, blank=True)
    replay = models.FileField(null=True, upload_to='replays', blank=True)
    players = models.ManyToManyField('Handle', through='Roster', related_name='matches')
    status = models.CharField(max_length=100, choices=STATUS, default=STATUS[0][0])
    ranked = models.BooleanField(default=False)
    arranged_teams = models.BooleanField(default=False)

    def __str__(self):
        if hasattr(self, 'match_players'):
            return self.match_players
        return str(self.id)

    def save(self, *args, **kwargs):
        """
        By default, django doesn't save CharFields as null, but rather than empty-string.
        Since I'm enforcing uniqueness on this field, two empty-strings will raise
        an IntegrityError. Set it to NONE (null) explicitly on model save.
        :param args:
        :param kwargs:
        :return:
        """
        if not self.game_id:
            self.game_id = None
        super(Match, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'matches'


class Roster(models.Model):
    created = models.DateTimeField(auto_created=True, auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    match = models.ForeignKey('Match', on_delete=models.CASCADE, related_name='rosters')
    handle = models.ForeignKey('Handle', on_delete=models.CASCADE, related_name='rosters')
    team_number = models.IntegerField(choices=TEAMS)
    position_number = models.IntegerField(choices=POSITIONS)
    color = models.CharField(max_length=50, blank=True, default='')

    def __str__(self):
        return f"{self.handle.name}"

    class Meta:
        unique_together = ('match', 'handle', 'team_number', 'position_number')

class MatchEvent(models.Model):
    created = models.DateTimeField(auto_created=True, auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    match = models.ForeignKey('Match', on_delete=models.CASCADE, related_name='match_events')
    handle = models.ForeignKey('Handle', on_delete=models.CASCADE, related_name='match_events')
    opposing_handle = models.ForeignKey('Handle', on_delete=models.SET_NULL, null=True, related_name="+")
    game_time = models.DecimalField(max_digits=12, decimal_places=4, null=True, blank=True)
    value = models.IntegerField(default=0)
    key = models.CharField(max_length=100, choices=EVENT_KEYS)
    raw = models.TextField(blank=True)
    description = models.TextField()
    points = models.IntegerField(default=0)

    def __str__(self):
        return f"{self.key}: {self.handle.name}"
