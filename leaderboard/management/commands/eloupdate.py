
import io
import re
import xml.etree.ElementTree as ET
from xml.etree.ElementTree import Element
import requests
from leaderboard.models import BankSync, Leaderboard
from django.core.management.base import BaseCommand
import logging
from typing import Optional, Any

log = logging.getLogger(__name__)




class Command(BaseCommand):

    def send_error(self, msg: str):
        """
        Just a helper since Heroku doesn't support writing to logs. This will be
        converted when we move providers.
        :param msg: The error message
        :return: None
        """
        log.error(msg)
        self.stdout.write(f"\n{msg}")

    def get_value(self, node: Element, key: str, player_number: int,
                  rtype: str ='int') -> Optional[Any]:
        full_key = f"p{player_number}{key}"
        value = node.find(f"Key[@name='{full_key}']/Value")
        if isinstance(value, Element):
            return value.attrib.get(rtype)
        else:
            error = f"Could not find {key} in node with player number {player_number}"
            self.send_error(error)

    def get_mode(self, node: Element) -> Optional[str]:
        key = "mode"
        value = node.find(f"Key[@name='{key}']/Value")
        if isinstance(value, Element):
            return value.attrib.get('string')
        else:
            error = f"Could not find MODE in node"
            self.send_error(error)

    def handle_to_profile_id(self, handle: str) -> Optional[int]:
        """Converts a starcraft Handle to their profile id
        """
        try:
            profile_id = handle.split('-')[-1]
            if profile_id.isdigit():
                return int(profile_id)
        except IndexError:
            error = f"Could not extract profile id from {handle}"
            log.error(f"Could not extract profile id from {handle}")
            self.stdout.write(error)
            return


    def parse_file(self, bank_file: str):
        url = bank_file.url
        r = requests.get(url, allow_redirects=True)
        data = io.BytesIO(r.content)
        tree = ET.parse(data)
        root = tree.getroot()
        lg = root.find("Section[@name='LG']")
        if lg is None:
            error = "Bank file has no Last Game Attribute. Not Parsing..."
            self.send_error(error)
            return
        if self.get_mode(lg) != '2v2v2v2':
            self.stdout.write('\nIgnoring Bank File. Game not 2v2v2v2')
            return

        for p in range(1, 9):
            games = self.get_value(lg, 'games', p)
            handle = self.get_value(lg, 'handle', p, rtype='string')
            wins = self.get_value(lg, 'wins', p)
            elo = self.get_value(lg, 'elo', p, rtype='fixed')
            if any(val is None for val in [games, handle, wins, elo]):
                error = f"Couldn't extract player information for Player {p}'"
                self.send_error(error)
                continue

            defaults = {
                'player_handle': handle,
                'games': games,
                'wins': wins,
                'elo': elo,
            }
            record, created = Leaderboard.objects.get_or_create(player_handle=handle, defaults=defaults)
            if not created:
                if int(games) > record.games:
                    for attr, value in defaults.items():
                        setattr(record, attr, value)
                    record.save()
            if record.player_name == "":
                profile_id = self.handle_to_profile_id(handle)

                record.player_name = self.get_player_name(profile_id)
                record.save()

    def get_player_name(self, profile_id: int, region=1):
        profile_url = f'https://starcraft2.com/en-us/profile/1/{region}/{profile_id}'
        r = requests.get(profile_url, allow_redirects=True)
        pattern = re.compile("<title>(.+?)</title>")
        matches = re.findall(pattern, r.content.decode('utf-8'))
        name = matches[0].split('-')
        name = name[1].strip()
        if name == "StarCraft II Official Game Site":
            return self.get_player_name(profile_id, region=2)
        return name

    def fix_handles(self):
        ls = Leaderboard.objects.filter(player_name='Starcraft II Official Game Site')
        for l in ls:
            name = self.get_player_name(self.handle_to_profile_id(l.player_handle))
            l.player_name = name
            l.save()


    def handle(self, *args, **options):
        for bank_file in BankSync.objects.all():
            self.stdout.write(f'\nReading from bank file {bank_file.url}')
            self.parse_file(bank_file)






