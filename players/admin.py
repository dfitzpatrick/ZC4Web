from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

from .forms import CustomPlayerCreationForm, CustomPlayerChangeForm
from .models import Player

class CustomPlayerAdmin(UserAdmin):
    add_form = CustomPlayerCreationForm
    form = CustomPlayerChangeForm
    model = Player
    list_display = ['username', 'id',]

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('invited_by')}
         ),
    )

admin.site.register(Player, CustomPlayerAdmin)
