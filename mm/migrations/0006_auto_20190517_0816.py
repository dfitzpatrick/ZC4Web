# Generated by Django 2.2 on 2019-05-17 15:16

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
from mm.models import Match


def set_game_id_to_null(apps, schema_editor):
    MatchModel = apps.get_model('mm', 'Match')
    for match in MatchModel.objects.filter(game_id=''):
        match.game_id = None
        match.save()


class Migration(migrations.Migration):

    dependencies = [
        ('mm', '0005_auto_20190512_0124'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='match',
            options={'verbose_name_plural': 'matches'},
        ),
        migrations.AlterField(
            model_name='handle',
            name='player',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='handles', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='match',
            name='game_id',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.RunPython(set_game_id_to_null),
        migrations.AlterField(
            model_name='match',
            name='game_id',
            field=models.CharField(blank=True, max_length=100, null=True, unique=True),
        ),
    ]
