from django.db import models
from players.models import Player
# Create your models here.


class Infraction(models.Model):
    created = models.DateTimeField(auto_created=True, auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    player = models.ForeignKey(Player, on_delete=models.CASCADE,
                               related_name='players')
    author = models.ForeignKey(Player, on_delete=models.CASCADE,
                               related_name='authors')
    reason = models.TextField()
