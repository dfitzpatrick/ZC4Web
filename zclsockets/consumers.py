from channels.generic.websocket import AsyncWebsocketConsumer
import json
from mm.models import Match, Handle, Roster, League, Season, MatchEvent
from channels.db import database_sync_to_async
from aioredis.errors import ConnectionClosedError
from .receivers import match_event
import logging
log = logging.getLogger(__name__)
class TestConsumer(AsyncWebsocketConsumer):

    async def connect(self):
        await self.channel_layer.group_add('mytest', self.channel_name)
        await self.accept()

    async def receive(self, text_data=None, bytes_data=None):
        try:
            j = json.loads(text_data)
            print("FIRE")
            await self.channel_layer.group_send('mytest', j)
        except json.JSONDecodeError:
            print("Error JSON")

    async def send(self, text_data=None, bytes_data=None, close=False):
        print("IN SEND")
        try:
            event = json.loads(text_data)
            if event.get('type'):
                event['response_type'] = event.pop('type')
            await super(TestConsumer, self).send(text_data, bytes_data, close)
        except json.JSONDecodeError:
            await super(TestConsumer, self).send(text_data, bytes_data, close)

    async def disconnect(self, event):
        print("disconnect", event)
        await self.channel_layer.group_discard(
            'mytest', self.channel_name)

    async def test_event(self, event):
        print("In Test Event")
        await self.send(text_data=json.dumps(event))




class MatchEventConsumer(AsyncWebsocketConsumer):

    async def connect(self):
        # The server is throwing random 500's. This appears to be a bug in AIOREDIS
        # THis will attempt to re-add the subscriber until it works.
        while True:
            try:
                group = self.scope['url_route']['kwargs']['streamer_id']
                self.group_name = group
                await self.channel_layer.group_add(self.group_name, self.channel_name)
                break
            except ConnectionClosedError as e:
                log.error(f"ConnectionClosed raised from Redis: {e}")
                continue
        await self.accept()


    async def receive(self, text_data=None, bytes_data=None):
        try:
            event = json.loads(text_data)
            if event.get('type') != 'twitch':
                result = await match_event.process(event)
            await self.channel_layer.group_send(self.group_name, event)
        except json.JSONDecodeError:
            print("Error on JSON")

    async def twitch(self, event):
        await self.send(text_data=json.dumps(event))

    async def player_died(self, event):
        await self.send(text_data=json.dumps(event))

    async def match_checkin(self, event):
        await self.send(text_data=json.dumps(event))

    async def match_checkout(self, event):
        await self.send(text_data=json.dumps(event))

    async def player_leave(self, event):
        await self.send(text_data=json.dumps(event))

    async def bunker_killed(self, event):
        await self.send(text_data=json.dumps(event))

    async def bunker_started(self, event):
        await self.send(text_data=json.dumps(event))

    async def match_end(self, event):
        await self.send(text_data=json.dumps(event))

    async def bunker_cancelled(self, event):
        await self.send(text_data=json.dumps(event))

    async def player_nuke(self, event):
        await self.send(text_data=json.dumps(event))

    async def match_start(self, event):
        await self.send(text_data=json.dumps(event))

    async def request_checkins(self, event):
        await self.send(text_data=json.dumps(event))

    async def response_checkins(self, event):
        await self.send(text_data=json.dumps(event))

    async def disconnect(self, event):
        print("disconnect", event)
        await self.channel_layer.group_discard(
            self.group_name, self.channel_name)

