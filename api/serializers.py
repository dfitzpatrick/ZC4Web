import logging

from drf_writable_nested import UniqueFieldsMixin, WritableNestedModelSerializer
from rest_framework import serializers

from match.models import Tournament, Match, Team, Event, League
from players.models import Player
from moderation.models import Infraction
from mm import models as mm_models

log = logging.getLogger(__name__)





class TournamentSerializer(WritableNestedModelSerializer):

    class Meta:
        model = Tournament
        fields = '__all__'


class PlayersSerializer(UniqueFieldsMixin, WritableNestedModelSerializer):
    class Meta:
        model = Player
        fields = ('id', 'username', 'display_name', 'discord_id',
                  'discord_discriminator', 'status', 'discord_avatar_url')


class TeamSerializer(WritableNestedModelSerializer):
    players = PlayersSerializer(many=True)

    class Meta:
        model = Team
        fields = '__all__'


class LeagueSerializer(WritableNestedModelSerializer):
    owner = PlayersSerializer()
    hosts = PlayersSerializer(many=True, required=False)
    membership = PlayersSerializer(many=True, required=False)

    class Meta:
        model = League
        fields = '__all__'


class EventSerializer(WritableNestedModelSerializer):
    host = PlayersSerializer()
    league = LeagueSerializer(required=False)
    players_interested = PlayersSerializer(many=True, required=False)
    players_checked_in = PlayersSerializer(many=True, required=False)

    class Meta:
        model = Event
        fields = '__all__'


class MatchSerializer(WritableNestedModelSerializer):
    tournament = TournamentSerializer(required=False)
    event = EventSerializer(required=False)
    league = LeagueSerializer(required=False)
    winning_team = TeamSerializer(required=False)
    teams = TeamSerializer(many=True)

    class Meta:
        model = Match
        fields = '__all__'


class InfractionSerializer(WritableNestedModelSerializer):
    player = PlayersSerializer(required=True)
    author = PlayersSerializer(required=True)

    class Meta:
        model = Infraction
        fields = '__all__'


class EventSummarySerializer(WritableNestedModelSerializer):
    host = PlayersSerializer()
    league = LeagueSerializer(required=False)
    event_matches = MatchSerializer(many=True)

    class Meta:
        model = Event
        fields = '__all__'

class HandleSerializer(WritableNestedModelSerializer):
    player = PlayersSerializer(WritableNestedModelSerializer)

    class Meta:
        model = mm_models.Handle
        fields = '__all__'
