from django.contrib import admin
from mm.models import *
from django.db.models import CharField, Value
from django.db.models.functions import Concat
import pytz
# Register your models here.



class MatchAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'id', 'created_field', 'league', 'season', 'status', 'ranked', 'arranged_teams']

    list_editable = ['league', 'season', 'ranked', 'status', 'arranged_teams']
    def get_queryset(self, request):

        return (
            super(MatchAdmin, self)
            .get_queryset(request)
            .select_related('league', 'season',)
            .prefetch_related('rosters')
            .annotate(match_players=StringAgg(
                'rosters__handle__name',
                delimiter=', ')
            )
        )

    def created_field(self, obj):
        ct_time = pytz.timezone('US/Central')
        created_date =  obj.created.astimezone(ct_time)
        return created_date.strftime("%a %b %d %Y %I:%M:%p %Z")


class MatchEventsAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'description']


    def get_queryset(self, request):
        return (
            super(MatchEventsAdmin, self)
            .get_queryset(request)
            .select_related('match', 'handle')
            .annotate(to_string=Concat(
                'handle__name', Value(': '), 'key'
                )
            )
        )

class RosterAdmin(admin.ModelAdmin):
    list_display = ['match', 'handle', 'team_number', 'position_number']

class HandleAdmin(admin.ModelAdmin):
    list_display = ['name', 'handle', 'player']
    ordering = ['name',
                ]
    def get_queryset(self, request):
        return (
            super(HandleAdmin, self).get_queryset(request)
            .select_related('player')
        )

admin.site.register(Match, MatchAdmin)
admin.site.register(Handle, HandleAdmin)
admin.site.register(MatchEvent, MatchEventsAdmin)
admin.site.register(Season)
admin.site.register(League)
admin.site.register(Roster, RosterAdmin)
