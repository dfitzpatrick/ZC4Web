from django.db import models
from players.models import Player
from match.models import Match
from typing import List
# Create your models here.


class PlayerStats(Player):

    def __init__(self, *args, **kwargs):
        super(PlayerStats, self).__init__(*args, **kwargs)
        self.matches = Match.objects.select_related(
            'tournament', 'league', 'event', 'winning_team',
        ).prefetch_related(
            'teams', 'teams__players', 'teams__team_matches', 'winning_team__players'
        ).filter(teams__players=self)


    class Meta:
        proxy = True
        app_label = 'core'

    def is_winner(self, match: Match) -> bool:
        if match.winning_team:
            return any(self.id == p.id for p in match.winning_team.players.all())


    def wins(self, matches: List[Match] = None) -> int:
        matches = matches if matches is not None else self.matches
        if matches:
            return sum(1 for m in matches if self.is_winner(m))
        return 0

    def draws(self, matches: List[Match] = None) -> int:
        matches = matches if matches is not None else self.matches
        if matches:
            return sum(1 for m in matches if m.winning_team is None)
        return 0

    def losses(self, matches: List[Match] = None) -> int:
        matches = matches if matches is not None else self.matches
        if matches:
            return len(matches) - self.wins(matches) - self.draws(matches)
        return 0

    def ratio(self,  matches: List[Match] = None) -> float:
        matches = matches if matches is not None else self.matches
        if matches:
            if self.losses(matches) > 0:
                return self.wins(matches) / len(matches)
        return 0

    def stats(self, matches: List[Match] = None) -> dict:
        matches = matches if matches is not None else self.matches
        if matches:
            return {
                'count': len(matches),
                'wins': self.wins(matches),
                'losses': self.losses(matches),
                'draws': self.draws(matches),
                'ratio': self.ratio(matches),
                'points': (self.wins(matches) * 3) - self.losses(matches) + self.draws(matches)
            }

