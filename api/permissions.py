from rest_framework import permissions

class IsStaffOrReadOnly(permissions.BasePermission):
    """
    Allows users to only edit if they is_staff == True
    """
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        return request.user.is_staff
