from django.db import models
from django.contrib.auth.models import AbstractUser



# Create your models here.
STATUS_CHOICES = (
    ('pending', 'PENDING INVITE'),
    ('confirmed', 'CONFIRMED INVITATION')
)


class Player(AbstractUser):
    discord_id = models.BigIntegerField(unique=True, null=True)
    discord_discriminator = models.CharField(max_length=20, blank=True)
    display_name = models.CharField(max_length=200, null=True)
    status = models.CharField(max_length=30, choices=STATUS_CHOICES,
                              default='pending')
    discord_avatar_url = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return self.username

