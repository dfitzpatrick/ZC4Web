from .models import Team, Event
from django import forms
import logging

log = logging.getLogger(__name__)

dateTimeOptions = {
            'format': 'yyyy/mm/dd HH:ii P',
            'autoclose': True,
            'showMeridian': True,
        }

class TeamAdminForm(forms.ModelForm):
    class Meta:
        exclude = ()
        model = Team

    def clean(self):
        players = self.cleaned_data.get('players')
        if self.instance.is_duplicate(players):
            log.warning(f"Tried to create duplicate team: {players}")
            raise forms.ValidationError(u'This team already exists.')


