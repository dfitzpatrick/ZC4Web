from django.shortcuts import render
from match.models import Team, Match, League, Tournament
from django.core import serializers
from core.stats import PlayerStatistics
from typing import List
from players.models import Player
import json
from core.stats import PlayerStatistics
from players.models import Player
from django.http import HttpResponse
# Create your views here.

def circles(request):
    try:
        global Player
        def get_players(m: Match) -> List[Player]:
            t:Team
            p: Player
            return [p for t in m.teams.all() for p in t.players.all()]

        from players.models import Player
        queryset = Match.objects.all().select_related('tournament', 'league', 'event', 'winning_team')
        match: Match = queryset.filter(status='in progress').order_by('-created').first()
        season_matches = queryset.filter(ranked=True, status='completed', tournament=match.tournament)
        teams_qs = match.teams.all()

        all_stats = PlayerStatistics(Player.objects.filter(team__in=teams_qs), season_matches)
        stats = [p for p in all_stats]

        players = get_players(match)
        players_json = serializers.serialize('json', players)
        #Some terrible stuff here
        d = json.loads(players_json)
        for p in d:

            p_obj = PlayerStatistics(p['pk'], season_matches)
            p_stats = p_obj.calculate_player()
            del p_stats['player']
            p['fields']['tournament'] = match.tournament.name
            p['fields']['stats'] = p_stats
            for rank, ps in enumerate(all_stats):
                if p['pk'] == ps['player'].id:
                    p['fields']['stats_ranking'] = rank + 1
                    p['fields']['current_win_streak'] = p_obj.current_win_streak()
                    p['fields']['longest_win_streak'] = p_obj.all_time_win_streak()

        players_json = json.dumps(d)

        context = {
            'teams': match.teams,
            'players': players,
            'players_js': players_json,

        }
        return render(request, 'zcallouts/circles.html', context)
    except:
        return HttpResponse("")

def current_match(request):
    pass


def circles_iter(request, players: List[Player]):
    player = players.pop(0)
    context = {
        'player': player,
        'players': players,
    }
    return render(request, 'zcallouts/circles.html', context)