from django.shortcuts import render, redirect
from match import models as match_models
from .models import PlayerStats
from players.models import Player
from core import stats
from core.forms import TournamentSelectForm
# Create your views here.
from django.conf import settings
import boto3

def index(request):
    events = match_models.Event.objects.filter(status='in progress').order_by('created').all()
    last_five_matches = match_models.Match.objects.filter(
        stream_url__isnull=False).exclude(stream_url__exact='').order_by('-created')[:5]
    context = {
        'events': events,
        'matches': last_five_matches
    }
    return render(request, 'core/index.html', context)


def stats_list(request):
    tournament = None
    form = TournamentSelectForm(request.GET or {'tournament':  match_models.Tournament.objects.filter(status='in progress').order_by('created').first().pk})
    if form.is_valid():
        tournament = form.cleaned_data['tournament']
    leagues = match_models.League.objects.all().order_by('name')
    data = {}
    for l in leagues:
        playerstats = stats.PlayerStatistics(Player.objects.all(), l.league_matches.filter(ranked=True, tournament=tournament), suppress_zero=True)
        data[l.name] = [p for p in playerstats if p.get('count', 0) > 0]

    context = {
        'data': data,
        'form': form,
        'tournament': tournament,
    }
    return render(request, 'core/stats.html', context)


def stats_detail(request, pk):
    tournament = None
    form = TournamentSelectForm(request.GET or {'tournament':  match_models.Tournament.objects.filter(status='in progress').order_by('created').first().pk})
    if form.is_valid():
        tournament = form.cleaned_data['tournament']
    playerstats = stats.PlayerStatistics.get(pk)
    player = playerstats.player
    lifetime_stats_ranked = playerstats.calculate_player(matches=playerstats.matches.filter(ranked=True))

    leagues = {}
    for l in player.league_membership.all():
        playerstats.filter_matches(teams__players=player, ranked=True, league=l, tournament=tournament)
        match_history = playerstats.match_history
        if match_history:
            leagues[l.name] = {}
            leagues[l.name]['matches'] = match_history
            leagues[l.name]['stats'] = playerstats.calculate_player()
            leagues[l.name]['current_win_streak'] = playerstats.current_win_streak()
            leagues[l.name]['all_time_win_streak'] = playerstats.all_time_win_streak()
            leagues[l.name]['teams'] = {}

            for t in player.team_set.filter(team_matches__league=l,
                                            team_matches__tournament=tournament,
                                            team_matches__ranked=True).all():
                playerstats.filter_matches(league=l, ranked=True, teams=t)
                leagues[l.name]['teams'][t] = playerstats.calculate_player()
    no_league = 'Non-League Matches'

    playerstats.filter_matches(teams__players=player, ranked=True, league=None, tournament=tournament)
    if playerstats.match_history:
        leagues[no_league] = {}
        leagues[no_league]['matches'] = playerstats.match_history
        leagues[no_league]['stats'] = playerstats.calculate_player()
        leagues[no_league]['teams'] = {}

        for t in player.team_set.filter(team_matches__league=None,
                                        team_matches__tournament=tournament,
                                        team_matches__ranked=True).all():
            playerstats.filter_matches(league=None, ranked=True, teams=t, tournament=tournament)
            leagues[no_league]['teams'][t] = playerstats.calculate_player()

    context = {
        'player': player,
        'lifetime_stats_ranked': lifetime_stats_ranked,
        'leagues': leagues,
        'form': form,
        'tournament': tournament or "No Tournament/Season",
    }
    return render(request, 'core/stats_detail.html', context)

def matchcli_latest(bucket):
    zips = [o for o in bucket.objects.all() if o.key.lower().endswith('.zip')]
    latest_version = None
    latest_object = None

    for f in zips:
        fn = f.key.strip('.zip')
        _, _, version = fn.split('-')
        if latest_version is None or version > latest_version:
            latest_version = version
            latest_object = f
    return latest_version, latest_object.key

def streaming_setup(request):

    client = boto3.client('s3')
    resource = boto3.resource('s3')
    bucket = resource.Bucket('matchcli')

    latest = matchcli_latest(bucket)
    path = 'https://matchcli.s3-us-west-1.amazonaws.com/{0}'.format(
        matchcli_latest(bucket)[1]
    )
    context = {
        'matchcli_version': latest[0],
        'matchcli_download_url': path,
    }

    return render(request, 'core/streaming.html', context)

def join(request):
    return redirect(settings.DISCORD_INVITE_URL)