from django.urls import path
from django.conf.urls.static import static
from django.conf import settings
from zclsockets import views
urlpatterns = [
    path('overlays/<int:id>/waiting', views.waiting, name='waiting'),
    path('overlays/<int:id>/alerter', views.alerter, name='alerter'),
    path('overlays/<int:id>/about', views.about_stream, name='about-stream'),
    path('overlays/<int:id>/ending', views.ending_screen, name='match-ending'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
