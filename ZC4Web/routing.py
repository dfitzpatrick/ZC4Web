from channels.routing import ProtocolTypeRouter
from zclsockets.routing import websocket_urlpatterns
from channels.routing import URLRouter
from channels.auth import AuthMiddlewareStack

application = ProtocolTypeRouter({
    'websocket': URLRouter(websocket_urlpatterns)
})
