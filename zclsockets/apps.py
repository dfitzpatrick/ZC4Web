from django.apps import AppConfig


class ZclsocketsConfig(AppConfig):
    name = 'zclsockets'
