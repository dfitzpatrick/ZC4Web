
import io
import re
import xml.etree.ElementTree as ET
from xml.etree.ElementTree import Element
import requests
from leaderboard.models import BankSync, Leaderboard
from django.core.management.base import BaseCommand
import logging
from typing import Optional, Any
import os
from leaderboard.models import Leaderboard
from mm.models import Handle, League, Season, Match, Roster, MatchEvent
from players.models import Player
import csv
from match.models import Match as OldMatch

log = logging.getLogger(__name__)


base_path = os.path.dirname(os.path.realpath(__file__))



def migrate_handles():
    # Move existing Leaderboard Handles to New Table
    ls = Leaderboard.objects.all()
    for l in ls:
        Handle.objects.create(
            handle=l.player_handle,
            name=l.player_name
        )

def create_leagues():
    names = ['Professional League', 'Intermediate League', 'Up and Coming League']
    for l in names:
        League.objects.create(name=l)

def create_seasons():
    names = ['Season 1', 'Season 2', 'Season 3']
    for s in names:
        Season.objects.create(name=s)

def get_points(outcome):
    if outcome == "WIN":
        return 5
    elif outcome == "LOSS":
        return 1
    else:
        return 0

def migrate_matches():
    matches = {}
    path = os.path.normpath(f"{base_path}/matches_test.csv")
    with open(path, 'r') as f:
        reader = csv.DictReader(f)
        for row in reader:
            old = OldMatch.objects.get(id=int(row.get('ID')))
            p = Player.objects.get(id=int(row.get("PLAYER ID")))
            handle = p.handles.first()
            if handle is None:
                handle = Handle.objects.create(
                    player=p,
                    handle=f"FAKE-{p.display_name}",
                    name=p.display_name,
                )

            m = matches.get(row.get('ID'))
            if m is None:
                s = Season.objects.get(name=row.get('SEASON'))
                l = League.objects.get(name=row.get('LEAGUE'))
                m = Match.objects.create(
                    season=s,
                    league=l,
                    status='completed',
                    ranked=True,
                )
                m.created = old.created
                m.updated = old.updated
                m.stream_url = old.stream_url
                m.save()
                matches[row.get('ID')] = m
            Roster.objects.create(
                match=m,
                handle=handle,
                team_number=int(row.get("TEAM NUMBER")),
                position_number=int(row.get("PLAYER NUMBER")),
            )
            MatchEvent.objects.create(
                match=m,
                handle=handle,
                key=row.get("OUTCOME"),
                description="Player Outcome",
                points=get_points(row.get("OUTCOME")),
            )



def map_profiles():
    path = os.path.normpath(f"{base_path}/migrate_players.csv")
    with open(path, 'r') as f:
        reader = csv.DictReader(f)
        for row in reader:
            try:
                p = Player.objects.get(id=int(row.get('ID')))
                h = Handle.objects.get(handle=row.get('HANDLE'))
                h.player = p
                h.save()
            except Handle.DoesNotExist:
                p = Player.objects.get(id=int(row.get('ID')))
                Handle.objects.create(
                    player = p,
                    handle = row.get('HANDLE'),
                    name = row.get('DISPLAYNAME'),
                )


class Command(BaseCommand):





    def linkprofiles(self):
        # Look if a handle exists, then link the profile using helper xls
        pass

    def handle(self, *args, **options):
        #create_seasons()
        #create_leagues()
        #migrate_handles()
        map_profiles()
        migrate_matches()





