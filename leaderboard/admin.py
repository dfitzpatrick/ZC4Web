from django.contrib import admin
from .models import BankSync, Leaderboard
# Register your models here.

class BankSyncAdmin(admin.ModelAdmin):
    list_display = ['url', 'memo']

class LeaderboardAdmin(admin.ModelAdmin):
    list_display = ['player_name', 'elo', 'updated']
    ordering = ['player_name']

admin.site.register(Leaderboard, LeaderboardAdmin)
admin.site.register(BankSync, BankSyncAdmin)