from django.urls import path
from mm import views
from mm import match_views
urlpatterns = [

    path('', views.list, name='stats'),
    path('detail/<pk>', views.list_detail, name='detail'),
    path('match', match_views.list_view, name='match_list'),
]