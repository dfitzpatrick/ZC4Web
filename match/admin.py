from django.contrib import admin
from django.contrib.admin.models import LogEntry
from .models import Tournament, Team, Match, Event, League
from .forms import TeamAdminForm

# Register your models here.

class TeamAdmin(admin.ModelAdmin):
    form = TeamAdminForm


class EventAdmin(admin.ModelAdmin):
    list_display = ['id', 'event_schedule', 'host', 'status']
    list_editable = ['event_schedule', 'status']
    ordering = ['-status', '-created']

class MatchAdmin(admin.ModelAdmin):
    list_display = ['id', '__str__', 'created', 'status', 'winning_team', 'ranked']
    ordering = ['-status', '-created']

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        queryset = queryset.select_related('tournament', 'league', 'winning_team')
        queryset = queryset.prefetch_related('teams', 'teams__players', 'winning_team__players')
        return queryset

class TournamentAdmin(admin.ModelAdmin):
    list_display = ['name', 'status']
    list_editable = ['status']

class LeagueAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'status']
    list_editable = ['status']

class LogEntryAdmin(admin.ModelAdmin):
    readonly_fields = ('content_type',
        'user',
        'action_time',
        'object_id',
        'object_repr',
        'action_flag',
        'change_message'
    )
    list_display = ['action_time', 'action_flag', 'content_type', 'object_id', 'user', 'change_message']
    def has_delete_permission(self, request, obj=None):
        return False

    def get_actions(self, request):
        actions = super(LogEntryAdmin, self).get_actions(request)
        del actions['delete_selected']
        return actions


#admin.site.register(Tournament, TournamentAdmin)
#admin.site.register(Team, TeamAdmin)
#admin.site.register(Match, MatchAdmin)
admin.site.register(Event, EventAdmin)
#admin.site.register(League, LeagueAdmin)
admin.site.register(LogEntry, LogEntryAdmin)
