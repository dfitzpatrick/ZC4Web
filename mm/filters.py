import django_filters
from mm.models import Handle, Season, League
from django.db.models import Count, Sum, Q, F, ExpressionWrapper, DecimalField, Value, Case, When
from decimal import Decimal


def handle_filter(request):
    if request is None:
        return Handle.objects.none()
    handles = request.user.filters
    season = request.user.season
    league = request.user.league

    if season is not None:
        handles = handles.filter(match_events__match__season__id=season)
    if league is not None:
        handles = handles.filter(match_events__match__league__id=league)
    return (
        handles
            .annotate(
            total_matches=Count(
                'match_events__match',
                distinct=True
            ),
            wins=Count(
                'match_events',
                filter=Q(match_events__key='WIN')
            ),
            losses=Count(
                'match_events',
                filter=Q(match_events__key='LOSS')
            ),
            draws=Count(
                'match_events',
                filter=Q(match_events__key='DRAW')
            ),
            points=Sum(
                'match_events__points'
            ),
        )
            .annotate(win_rate=Case(
            When(total_matches=0, then=0),
            default=(Decimal('1.0') * F("wins") / F("total_matches")) * 100,
            output_field=DecimalField(),
        )
        )
            .filter(total_matches__gte=1)
            .order_by('-points')
    )



class HandleMatchFilter(django_filters.FilterSet):
    matches__match__season__id= django_filters.ModelChoiceFilter(queryset=Season.objects.all())
    matches__match__league__id = django_filters.ModelChoiceFilter(queryset=League.objects.all())

    class Meta:
        model = Handle
        fields = {
            'name': ['icontains'],
        }


