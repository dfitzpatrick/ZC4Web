from django.contrib import admin
from moderation import models
# Register your models here.


class InfractionAdmin(admin.ModelAdmin):
    list_display = ['player', 'author', 'created', 'reason']

    def get_queryset(self, request):
        queryset = super(InfractionAdmin, self).get_queryset(request)
        queryset.select_related('player', 'author')
        return queryset

admin.site.register(models.Infraction, InfractionAdmin)
