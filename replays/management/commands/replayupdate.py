from django.core.management.base import BaseCommand

log = logging.getLogger(__name__)

class Command(BaseCommand):

    def handle(self, *args, **options):
        """
        Process Replay Imports

        Parameters
        ----------
        args
        options

        Returns
        -------

        """
