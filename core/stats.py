from typing import List
from django.db.models.query import QuerySet
from django.db.models import Manager
from typing import Union
from match.models import Match, League
from players.models import Player



class PlayerStatistics:
    def __init__(self, players: Union[QuerySet, int], matches: QuerySet = None, suppress_zero: bool = False) -> None:
        """Sets up default prefetching to reduce considerable amounts of queries.
        That way in views we can mainly approach it nromally without duplicating prefetches.
        """
        self.single_player_instance = None
        if isinstance(players, QuerySet) or isinstance(players,Manager):
            self.players = players.prefetch_related('team_set').all()
        elif isinstance(players, int):
            self.single_player_instance = Player.objects.prefetch_related('team_set').get(id=players)
            self.players = [self.single_player_instance]
            if matches is None:
                self.matches = Match.objects.select_related(
                    'tournament', 'league', 'event', 'winning_team',
                ).prefetch_related(
                    'teams', 'teams__players', 'teams__team_matches', 'winning_team__players',
                ).filter(teams__players=self.single_player_instance)

        else:
            raise Exception("Players not a queryset or int")
        if matches is not None:
            self.matches = matches.select_related('winning_team').all()
        else:
            self.matches = None
        self.suppress_zero = suppress_zero
        self._matches_initial = self.matches

    @classmethod
    def get(cls, player_id: int, **kwargs):
        matches = kwargs.get('matches')
        if matches is None:
            kwargs['matches'] = Match.objects.select_related(
                'tournament', 'league', 'event', 'winning_team',
            ).prefetch_related(
                'teams', 'teams__players', 'teams__team_matches', 'winning_team__players'
            ).filter(teams__players__id=player_id)
        return cls(int(player_id), **kwargs)

    def filter_matches(self, **kwargs):
        self.matches = self._matches_initial.filter(**kwargs)

    @property
    def player(self):
        return self.single_player_instance


    def match_outcome(self, player: Player, match: Match) -> str:
        if match.winning_team is None:
            return 'DRAW'
        return 'WIN' if self.is_a_winner(player, match) else 'LOSS'



    @property
    def match_history(self):
        if self.single_player_instance:
            self.matches = self.matches.order_by('-created')
            return [(m, self.match_outcome(self.single_player_instance, m))
                    for m in self.matches]


    def current_win_streak(self, d=None):
        if d is None:
            d = [outcome[1] for outcome in self.match_history]
        for num_wins, outcome in enumerate(d):
            if outcome != 'WIN':
                # Don't add 1 because we've technically iterated one extra time.
                return num_wins
        else:
            return len(d)

    def all_time_win_streak(self, d=None):
        if d is None:
            d = [outcome[1] for outcome in self.match_history]
        streaks = []
        while len(d) >= 1:
            streaks.append(self.current_win_streak(d))
            d = d[streaks[-1] + 1:]
        return max(streaks)

    def is_a_winner(self, player: Player, match: Match) -> bool:
        return match.winning_team in player.team_set.all()

    def calculate_player(self, player: Player = None, matches: QuerySet = None) -> dict:
        if player is None:
            player = self.single_player_instance
            if player is None:
                # Still none which means it was a queryset passed in.
                raise Exception("Missing Player Argument to calculate_player() does not have a single instance.")

        if matches is None:
            matches = self.matches
            if matches is None:
                raise Exception("Missing Match Argument to calculate_player() does not have matches associated.")

        player_matches = matches.filter(teams__players=player) or []
        pwins = sum(1 for m in player_matches if self.is_a_winner(player, m))
        pdraws = sum(1 for m in player_matches if m.winning_team is None)
        plosses = len(player_matches) - pwins - pdraws
        ppoints = (pwins * 5) + plosses
        try:
            pratio = (pwins / (pwins + plosses + pdraws)) * 100
        except ZeroDivisionError:
            pratio = 0

        return {
            'player': player,
            'count': len(player_matches),
            'wins': pwins,
            'losses': plosses,
            'draws': pdraws,
            'points': ppoints,
            'ratio': pratio,
        }

    def __iter__(self):
        vals = sorted([self.calculate_player(p) for p in self.players], key=lambda k: k['points'], reverse=True)
        if self.suppress_zero:
            vals = [o for o in vals if o.get('count', 0) > 0]
        return iter(vals)



