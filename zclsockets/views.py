from django.shortcuts import render
from leaderboard.models import Leaderboard
from decimal import Decimal
from django.shortcuts import render
from django.db.models import F, DecimalField, When, Case, IntegerField

from leaderboard.models import Leaderboard
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models.functions import Rank
from django.db.models.expressions import Window
import re
from datetime import timedelta, datetime
import pytz
from datetime import date
from django.contrib.postgres.aggregates import StringAgg
from django.db.models import Count, Sum, Q, F, ExpressionWrapper, DecimalField, Value, Case, When

from mm.models import Match, Handle, MatchEvent
from players.models import Player

# Create your views here.
def waiting(request, id):
    rank_window = Window(expression=Rank(),
                         order_by=F('elo').desc())
    match = Match.objects.filter(streamer_id=id).last()
    streamer = Player.objects.get(discord_id=id)
    handles = (
        Handle
        .objects

        .annotate(
            total_matches=Count(
                'match_events__match',
                distinct=True
            ),
            wins=Count(
                'match_events',

                filter=Q(match_events__key='WIN'),
            ),
            losses=Count(
                'match_events',
                filter=Q(match_events__key='LOSS')
            ),
            draws=Count(
                'match_events',
                filter=Q(match_events__key='DRAW')
            ),
            points=Sum(
                'match_events__points'
            ),
        )
        .annotate(win_rate=Case(
                When(total_matches=0, then=0),
                default=(Decimal('1.0') * F("wins") / F("total_matches")) * 100,
                output_field=DecimalField(),
            )
        )
        .filter(match_events__match=match, match_events__key='WIN')
    )
    today = date.today()
    session_info = (
        Match
        .objects
        .filter(
            streamer_id=id,
            created__year=today.year,
            created__month=today.month,
            created__day=today.day,

        )
        .annotate(
            match_players=StringAgg(
                'rosters__handle__name',
                delimiter=', '
            )
        )
    )

    leaderboard = (
        Leaderboard
            .objects

            .order_by('-elo')
            .annotate(losses=F("games") - F("wins"))
            .annotate(rank=rank_window)
            .annotate(win_rate=Case(
            When(games=0, then=0),
            default=(Decimal('1.0') * F("wins") / F("games")) * 100,
            output_field=DecimalField(),
            )
        )
    )
    status = match.status if match is not None else 'completed'
    context = {
        'leaderboard': leaderboard.all()[:5],
        'winners': handles,
        'matches': session_info,
        'status': status,
        'player': streamer,
    }
    return render(request, 'zclsockets/new_waiting.html', context)

def alerter(request, id):
    return render(request, 'zclsockets/alerter.html', {})

def about_stream(request, id):
    try:
        streamer = Player.objects.get(discord_id=id)
    except Player.DoesNotExist:
        streamer = None

    context = {
        'streamer': streamer,
    }
    return render(request, 'zclsockets/about.html', context)


def ending_screen(request, id):
    game_id = request.GET.get('show')
    try:
        match = Match.objects.prefetch_related('match_events').filter(streamer_id=id).order_by('created')
        if game_id:
            match = match.get(game_id=game_id)
        else:
            match = match.last()
    except Match.DoesNotExist:
        match = None
    winners = None
    league = None
    season = None

    if match:
        winners = ' & '.join(me.handle.name for me in match.match_events.filter(key='WIN')) or "No Winners"
        league = match.league
        season = match.season
    if isinstance(winners, str):
        winners = winners.upper()

    context = {
        'winners': winners or "No Winners",
        'league': league or "No League",
        'season': season or "No Season",
    }
    return render(request, 'zclsockets/ending.html', context)
