import json
from mm.models import Match, Handle, Roster, League, Season, MatchEvent
from typing import Optional, Any, Dict, List
from channels.db import database_sync_to_async
from django.db.models import F
import logging
from helpers.handles import get_player_name, handle_to_profile_id
from decimal import Decimal

log = logging.getLogger(__name__)

def get_bunker_type(index: int) -> str:
    marine = [
        1,2,3,4,5,6,8,15,16,23,24,31,32,39,40,47,48,55,57,58,59,60,61,62
        ]
    reaper = [
        9,10,11,12,13,14,17,22,25,30,33,38,41,46,49,50,51,52,53,54
    ]
    marauder = [
        18,19,20,21,26,29,34,37,42,43,44,45,
    ]
    ghost = [
        27,28,35,36
    ]
    if index in marine:
        return "Marine"
    elif index in reaper:
        return "Reaper"
    elif index in marauder:
        return "Marauder"
    elif index in ghost:
        return "Ghost"
    else:
        return "Unknown"

async def process(event: Dict[str, Any]) -> Optional[Any]:
    """
    Looks to see if the text_data is a json-like string and dispatches to various methods
    based on the 'type' key
    :param text_data:
    :return:
    """
    func = {
        'match_start': match_start,
        'match_end': match_end,
        'bunker_started': bunker_started,
        'bunker_killed': bunker_killed,
        'bunker_cancelled': bunker_cancelled,
        'player_nuke': player_nuke,
        'player_leave': player_leave,
    }
    if event is None:
        return
    try:
        key = event.get('type')
        if key:
            return await func[key](event)

    except json.JSONDecodeError:
        log.debug(f"Could not convert event to JSON {event}")
        return
    except KeyError:
        log.debug(f"No defined function for {key}")
        return

async def match_start(event: Dict[str, Any]):
    players = event.get('players', [])
    color = event.get('color', '')
    if players:
        m = await create_match(event)

        for p in players:
            defaults = {'handle': p.get('handle'), 'name': 'FOO'}
            handle = await get_or_create_handle(p.get('handle'), defaults)
            await create_roster(match=m,
                                handle=handle,
                                team_number=p.get('team'),
                                position_number=p.get('slot'),
                                color=color
            )
        # TODO: Serialize entire match object

        event['player_info'] = await player_details(m)
        event['unlinked_handles'] = await unlinked_handles(m)
        event['observer_info'] = await observer_details(event['observers'])


async def match_end(event: Dict[str, Any]):
    await set_outcomes(event)


async def bunker_started(event: Dict[str, Any]):
    await record_bunker_started(event)


async def bunker_killed(event: Dict[str, Any]):
    await record_bunker_killed(event)


async def bunker_cancelled(event: Dict[str, Any]):
    await record_bunker_cancelled(event)


async def player_nuke(event: Dict[str, Any]):
    await record_player_nuke(event)


async def player_leave(event: Dict[str, Any]):
    await record_player_leave(event)

@database_sync_to_async
def unlinked_handles(match):
    hs = match.rosters.filter(handle__player=None).values('handle__handle', 'handle__name')
    return list(hs)


@database_sync_to_async
def observer_details(observers: List[Dict[str, str]]):
    container = []
    for o in observers:
        handle = o.get('handle')
        if handle == '':
            continue
        try:
            h = Handle.objects.get(handle=handle)
            container.append(h.player.discord_id)
        except Handle.DoesNotExist or Handle.MultipleObjectsReturned:
            continue
    log.info(f"Observer details: {container}")
    return container


@database_sync_to_async
def player_details(match: Match):
    players = (
        match
        .rosters
        .annotate(
            name=F("handle__name"),
            handle_string=F("handle__handle"),
            discord_id=F("handle__player__discord_id")
        )
        .values('name', 'handle_string', 'discord_id', 'team_number')
    )
    return list(players)


@database_sync_to_async
def create_match(event):
    print("Creating Match")
    streamer_id = event.get('streamer_id') or ''
    game_id = event.get('game_id') or ''
    league = None
    season = None
    try:
        league_id = event.get('league')
        season_id = event.get('season')
        if league_id:
            league = League.objects.get(id=league_id)
        if season_id:
            season = Season.objects.get(id=season_id)
    except League.DoesNotExist:
        league = None
    except Season.DoesNotExist:
        season = None
    defaults = {
        'streamer_id': streamer_id,
        'league': league,
        'season': season,
        'ranked': False,
    }
    m, created = Match.objects.get_or_create(
        game_id=game_id,
        defaults=defaults
    )
    # TODO: Serialize match object to this.
    event['match_id'] = m.id
    return m

@database_sync_to_async
def get_or_create_handle(handle, defaults):
    print("Getting handle...")
    obj, created = Handle.objects.get_or_create(
        handle=handle,
        defaults=defaults
    )
    if created:
        name = get_player_name(handle_to_profile_id(obj.handle))
        obj.name = name
        obj.save()
    return obj

@database_sync_to_async
def create_roster(match, handle, team_number, position_number, color=''):
    """
    In case there are multiple streamers that have the script running in the same
    game we will check to see if this type of record exists in the db first.
    If so, it'll not create the match but return it.
    :param match:
    :param handle:
    :param team_number:
    :param position_number:
    :param color:
    :return:
    """
    defaults = {
        'color': color,
    }
    r, created = Roster.objects.get_or_create(
        match=match,
        handle=handle,
        team_number=team_number,
        position_number=position_number,
        defaults=defaults,
    )
    return r

@database_sync_to_async
def set_outcomes(event):
    try:
        event['winners'] = []
        print("Setting winners")
        winning_team = event.get('winning_team')
        if not winning_team.isdigit():
            # It was a string, therefore, draw
            # TODO: This will make it so all players get a draw. Open to implement
            # TODO: later if we want only surviving players.
            winning_team = -1
        else:
            winning_team = int(winning_team)

        game_time = get_game_time(event.get('time'))
        game_id = event.get('game_id')
        match = Match.objects.get(game_id=game_id)
        match.status = 'completed'
        match.save()
        event['match_id'] = match.id
        rosters = match.rosters
        for r in rosters.all():
            print(f"Updating for {r.handle.name}")
            if winning_team == -1:
                # Make sure to not duplicate if other streamers
                defaults = {
                    'raw': json.dumps(event),
                    'description': 'Player Outcome',
                    'points': 0,
                }
                try:
                    MatchEvent.objects.get_or_create(
                        match=match,
                        handle=r.handle,
                        key='DRAW',
                        game_time=game_time,
                        defaults=defaults
                    )
                except MatchEvent.MultipleObjectsReturned as e:
                    # This could be Training testing with FOO. Either way log and ignore
                    log.error(e)

            elif r.team_number == winning_team:
                # Make sure to not duplicate if other streamers
                defaults = {
                    'raw': json.dumps(event),
                    'description': 'Player Outcome',
                    'points': 5,
                }
                winner, created = MatchEvent.objects.get_or_create(
                    match=match,
                    handle=r.handle,
                    key='WIN',
                    game_time=game_time,
                    defaults=defaults
                )
                obj = {
                    'match_id': match.id,
                    'handle': winner.handle.handle,
                    'name': winner.handle.name,
                    }
                if winner.handle.player is not None:
                    obj['discord_id'] = winner.handle.player.discord_id
                else:
                    obj['discord_id'] = None
                event['winners'].append(obj)

            else:
                # Make sure to not duplicate if other streamers
                defaults = {
                    'raw': json.dumps(event),
                    'description': 'Player Outcome',
                    'points': 1,
                }
                MatchEvent.objects.get_or_create(
                    match=match,
                    handle=r.handle,
                    key='LOSS',
                    game_time=game_time,
                    defaults=defaults
                )
    except Match.DoesNotExist:
        log.debug(f"Error Could not find match with game id {game_id}")
        print(f"Error Could not find match with game id {game_id}")

def get_game_time(t: Any) -> Optional[Decimal]:
    game_time = t
    try:
        game_time = Decimal(game_time)
        # Game runs on 1.4 game time
        game_time = round(game_time / Decimal('1.40'), 2)
    except:
        return None
    return game_time

@database_sync_to_async
def record_bunker_killed(event):
    try:
        owner = Handle.objects.get(handle=event.get('owner'))
        match = Match.objects.get(game_id=event.get('game_id'))
        index = event.get('index')
        bunker_type = get_bunker_type(index)
        game_time = get_game_time(event.get('time'))
        # killer could return an empty string from the json response. Probably
        # some type of environmental / tank splash damage kill
        killer = None
        killer_handle = event.get('killer', '')
        if killer_handle != '':
            killer = Handle.objects.get(handle=event.get('killer'))
            description = f"{killer.name} Destroyed {owner.name}'s {bunker_type} Bunker"
            event['alert_avatar'] = get_avatar(killer)
        else:
            description = f"{owner.name}'s {bunker_type} Bunker was Destroyed"
            event['alert_avatar'] = get_avatar(owner)
        event['alert_message'] = description

        # Make sure not to duplicate it if there was another streamer
        defaults = {
            'raw': json.dumps(event),
            'description': description,
        }
        MatchEvent.objects.get_or_create(
            match=match,
            handle=owner,
            opposing_handle=killer,
            key='bunker_killed',
            game_time=game_time,
            defaults=defaults
        )
    except Handle.DoesNotExist as e:
        print(f"Error couldn't find handle {e} {owner} {killer}")
        log.debug(f"Error couldn't find handle {e} {owner} {killer} {event}")
    except Match.DoesNotExist as e:
        print(f"Error couldn't find match {e}")
        log.debug(f"Error couldn't find match {e}")

@database_sync_to_async
def record_bunker_started(event):
    try:
        owner = Handle.objects.get(handle=event.get('player'))
        match = Match.objects.get(game_id=event.get('game_id'))
        index = event.get('index')
        bunker_type = get_bunker_type(index)
        game_time = get_game_time(event.get('time'))
        description = f"{owner.name} Starts a {bunker_type} Bunker"
        # Make sure not to duplicate it if there was another streamer
        defaults = {
            'raw': json.dumps(event),
            'description': description,
        }
        MatchEvent.objects.get_or_create(
            match=match,
            handle=owner,
            key='bunker_started',
            game_time=game_time,
            defaults=defaults
        )
        event['alert_message'] = description
        event['alert_avatar'] = get_avatar(owner)
    except Handle.DoesNotExist as e:
        print(f"Error couldn't find handle {e}")
        log.debug(f"Error couldn't find handle {e} {owner} {event}")
    except Match.DoesNotExist as e:
        print(f"Error couldn't find match {e}")

@database_sync_to_async
def record_player_nuke(event):
    try:
        owner = Handle.objects.get(handle=event.get('player'))
        match = Match.objects.get(game_id=event.get('game_id'))
        game_time = get_game_time(event.get('time'))
        value = event.get('value')
        description = f"{owner.name} Nukes bringing in a value of {value}"
        # Make sure not to duplicate it if there was another streamer
        defaults = {
            'raw': json.dumps(event),
            'description': description,
        }
        MatchEvent.objects.get_or_create(
            match=match,
            handle=owner,
            key='player_nuke',
            game_time=game_time,
            defaults=defaults
        )
        event['alert_message'] = description
        event['alert_avatar'] = get_avatar(owner)
    except Handle.DoesNotExist as e:
        print(f"Error couldn't find handle {e}")
        log.debug(f"Error couldn't find handle {e} {owner} {event}")
    except Match.DoesNotExist as e:
        print(f"Error couldn't find match {e}")

@database_sync_to_async
def record_player_leave(event):
    try:
        owner = Handle.objects.get(handle=event.get('player'))
        match = Match.objects.get(game_id=event.get('game_id'))
        game_time = get_game_time(event.get('time'))
        description = f"{owner.name} Has Left The Game"
        # Make sure not to duplicate it if there was another streamer
        defaults = {
            'raw': json.dumps(event),
            'description': description,
        }
        MatchEvent.objects.get_or_create(
            match=match,
            handle=owner,
            key='player_leave',
            game_time=game_time,
            defaults=defaults
        )
        event['alert_message'] = description
        event['alert_avatar'] = get_avatar(owner)
    except Handle.DoesNotExist as e:
        print(f"Error couldn't find handle {e}")
        log.debug(f"Error couldn't find handle {e} {owner} {event}")
    except Match.DoesNotExist as e:
        print(f"Error couldn't find match {e}")



@database_sync_to_async
def record_bunker_cancelled(event):
    try:
        owner = Handle.objects.get(handle=event.get('player'))
        match = Match.objects.get(game_id=event.get('game_id'))
        index = event.get('index')
        bunker_type = get_bunker_type(index)
        game_time = get_game_time(event.get('time'))
        description = f"{owner.name} Cancelled Their {bunker_type} Bunker"
        # Make sure not to duplicate it if there was another streamer
        defaults = {
            'raw': json.dumps(event),
            'description': description,
        }
        MatchEvent.objects.get_or_create(
            match=match,
            handle=owner,
            key='bunker_cancelled',
            game_time=game_time,
            defaults=defaults
        )
        event['alert_message'] = description
        event['alert_avatar'] = get_avatar(owner)

    except Handle.DoesNotExist as e:
        print(f"Error couldn't find handle {e}")
        log.debug(f"Error couldn't find handle {e} {owner} {event}")
    except Match.DoesNotExist as e:
        print(f"Error couldn't find match {e}")


def get_avatar(handle: Handle) -> str:
    default = 'https://cdn.discordapp.com/embed/avatars/1.png'
    profile = handle.player
    if profile is not None:
        if profile.discord_avatar_url:
            return profile.discord_avatar_url
    return default

