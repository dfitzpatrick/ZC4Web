from .models import Player
from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm


class CustomPlayerCreationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = Player
        fields = ('username', 'email')

class CustomPlayerChangeForm(UserChangeForm):

    class Meta:
        model = Player
        fields = ('id', 'username', 'email',)
