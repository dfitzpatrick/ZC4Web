from mm.models import Handle
from leaderboard.models import Leaderboard
from typing import Optional
import requests
import logging
import re

log = logging.getLogger(__name__)

def handle_to_profile_id(handle: str) -> Optional[int]:
    """Converts a starcraft Handle to their profile id
    """
    try:
        profile_id = handle.split('-')[-1]
        if profile_id.isdigit():
            return int(profile_id)
    except IndexError:
        error = f"Could not extract profile id from {handle}"
        log.error(f"Could not extract profile id from {handle}")
        return

def get_player_name(profile_id: int, region=1):
    profile_url = f'https://starcraft2.com/en-us/profile/1/{region}/{profile_id}'
    r = requests.get(profile_url, allow_redirects=True)
    pattern = re.compile("<title>(.+?)</title>")
    matches = re.findall(pattern, r.content.decode('utf-8'))
    name = matches[0].split('-')
    name = name[1].strip()
    if name == "StarCraft II Official Game Site":
        return get_player_name(profile_id, region=2)
    return name


def fix_leaderboard_handles():
    ls = Leaderboard.objects.filter(player_name='StarCraft II Official Game Site')
    for l in ls:
        name = get_player_name(handle_to_profile_id(l.player_handle))
        l.player_name = name
        l.save()

def fix_handles():
    hs = Handle.objects.filter(name='StarCraft II Official Game Site')
    for h in hs:
        name = get_player_name(handle_to_profile_id(h.handle))
        h.name = name
        h.save()
