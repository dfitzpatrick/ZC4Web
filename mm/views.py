from django.shortcuts import render
from mm.models import Handle, League
from django.db.models import Count, Sum, Q, F, ExpressionWrapper, DecimalField, Value, Case, When
from decimal import Decimal
from .filters import HandleMatchFilter
from .forms import LeagueFilterForm
from django.db.models.functions import Rank
from django.db.models.expressions import Window
from players.models import Player
from typing import Optional
from mm.models import Season, League

# Create your views here.
def list(request):
    rank_window = Window(expression=Rank(),
                         order_by=F('points').desc())

    handles = Handle.objects.all()
    default = {
        'league': League.objects.first().id
    }
    form = LeagueFilterForm(request.GET or default)
    if form.is_valid():
        season = request.GET.get('season')
        league = request.GET.get('league')
        name = request.GET.get('name')
        # I still dont fully understand this, but if you simply chain .filter() we will
        # not get the queryset we intend when we annotate. Here we must do one .filter()
        # which will make it work more like AND
        # This took forever to find out
        # https://stackoverflow.com/questions/8164675/chaining-multiple-filter-in-django-is-this-a-bug
        custom_filter = {}
        if name:
            custom_filter['name__icontains'] = name
        if season:
            custom_filter['match_events__match__season'] = season

        if league:
            custom_filter['match_events__match__league'] = league
        handles = handles.filter(**custom_filter)

    handles = (
            handles
            .annotate(
                total_matches=Count(
                    'match_events__match',
                    distinct=True
                ),
                wins=Count(
                    'match_events',

                    filter=Q(match_events__key='WIN'),
                ),
                losses=Count(
                    'match_events',
                    filter=Q(match_events__key='LOSS')
                ),
                draws=Count(
                    'match_events',
                    filter=Q(match_events__key='DRAW')
                ),
                points=Sum(
                    'match_events__points'
                ),
            )
            .annotate(rank=rank_window)
            .annotate(win_rate=Case(
                When(total_matches=0, then=0),
                default=(Decimal('1.0') * F("wins") / F("total_matches")) * 100,
                output_field=DecimalField(),
                )
            )
            .filter(total_matches__gte=1)
            .order_by('-points')
        )


    context = {
        'handles': handles,
        'form': form,
    }
    return render(request, "mm/list.html", context)

def get_avatar(profile: Optional[Player]) -> str:
    default = 'https://cdn.discordapp.com/embed/avatars/1.png'
    if isinstance(profile, Player):
        return profile.discord_avatar_url or default
    return default

def list_detail(request, pk):

    handle = Handle.objects.get(pk=pk)
    profile = handle.player
    avatar = get_avatar(profile)
    context = {
        'handle': handle,
        'profile': profile,
        'avatar': avatar,
    }

    return render(request, 'mm/list-detail.html', context)

