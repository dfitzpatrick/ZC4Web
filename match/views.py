from django.shortcuts import render
from .models import Match, Team
# Create your views here.
from django.views.generic import DetailView

class MatchDetailView(DetailView):
    model = Match

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        match = context['match']
        context['league'] = match.league.name or "Open to All Players"
        context['tournament'] = match.tournament.name or "Ad-hoc Match"
        context['game_number'] = Match.objects.filter(created__lte=match.created,
                                                      tournament=match.tournament,
                                                      league=match.league,
                                                      ranked=True).count()


        return context


class TeamDetailView(DetailView):
    model = Team

