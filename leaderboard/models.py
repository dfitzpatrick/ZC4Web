from django.db import models

class BankSync(models.Model):
    url = models.URLField()
    memo = models.CharField(max_length=300, blank=True)

    def __str__(self):
        return self.url

    class Meta:
        app_label = 'leaderboard'

class Leaderboard(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    player_handle = models.CharField(max_length=300)
    player_name = models.CharField(max_length=300, blank=True)
    games = models.IntegerField()
    elo = models.FloatField()
    wins = models.IntegerField()

    def __str__(self):
        return self.player_name or "(Unresolved Player Name)"

    class Meta:
        app_label = 'leaderboard'


